import time

from kivy.app import App
from kivy.core.window import Window
from kivy.properties import ListProperty
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.screenmanager import Screen
from kivy.uix.scrollview import ScrollView

from widgets.otp_widget import OTPWidget


class ControlPanel(App):
    otp_codes = ListProperty([])

    def new_account(self, pressed_btn):
        btn = OTPWidget("Gmail")
        self.msg_layout.add_widget(btn)
        self.sv1_main.scroll_to(btn)


    def build_right_column(self):
        self.sv1_main = ScrollView(pos_hint = {"top":1, "center_x":0.75},
                                    size_hint = (0.49, 0.85))
        self.msg_layout = GridLayout(cols = 1,
                                     size_hint_y = None)
        self.msg_layout.bind(minimum_height = self.msg_layout.setter('height'))
        self.sv1_main.add_widget(self.msg_layout)
        self.msg_layout.add_widget(OTPWidget("Some account"))

    def build(self):
        self.scr = Screen()
        self.build_right_column()
        self.bt1_main = Button(text = "New Account",
                               size_hint = (0.12, 0.078),
                               pos_hint = {"top":0.097, "center_x":0.927},
                               on_press = self.new_account)

        self.scr.add_widget(self.sv1_main)
        self.scr.add_widget(self.bt1_main)

        return self.scr

ControlPanel().run()
