scp main.py t@$1:~/ras-display
scp -r widgets/ t@$1:~/ras-display
scp requirements.txt t@$1:~/ras-display
ssh -t t@$1 'cd ras-display && source ~/.virtualenvs/ras-display/bin/activate && pip install -r requirements.txt && KCFG_KIVY_LOG_LEVEL=debug python3 main.py'