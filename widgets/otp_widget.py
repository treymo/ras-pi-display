import pyotp
from threading import Thread
import time

from kivy.logger import Logger
from kivy.uix.button import Button


class OTPWidget(Button):
    def __init__(self, account_name):
        self.account_name = account_name
        self.size_hint_y = None
        self.height = 75
        self.background_color = (1,1,1,1)

        base32secret = 'S3K3TPI5MYA2M67V'
        self.totp = pyotp.TOTP(base32secret)
        self.otp_code = self.totp.now()

        self.text = self.get_button_text()

        self.updater = Thread(target=self.worker)
        self.updater.start()

        super(OTPWidget, self).__init__()
    
    def get_button_text(self):
        return self.account_name + ": " + self.otp_code
    
    def worker(self):
        while True:
            time.sleep(2) # blocking operation
            otp_code = self.totp.now()
            if otp_code != self.otp_code:
                self.otp_code = otp_code
                Logger.info("new OTP for account '" + self.account_name + "': " + otp_code + "\n")
                self.text = self.get_button_text()